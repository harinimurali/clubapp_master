package com.dci.clupapp.dagger;

import android.content.SharedPreferences;

import com.dci.clupapp.activity.LoginActivity;
import com.dci.clupapp.activity.NotificationActivity;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.fragment.AboutFragment;
import com.dci.clupapp.fragment.ConnectFragment;
import com.dci.clupapp.fragment.ContactFragment;
import com.dci.clupapp.fragment.EventFragment;
import com.dci.clupapp.fragment.HomeFragment;
import com.dci.clupapp.fragment.MemberlistFragment;
import com.dci.clupapp.fragment.NewsFragment;
import com.dci.clupapp.fragment.OtpVerificationFragment;
import com.dci.clupapp.fragment.ProjectListFragment;
import com.dci.clupapp.retrofit.RetrofitModule;


import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    void inject(ClubApplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();

    void inject(OtpVerificationFragment otpVerificationFragment);

    void inject(LoginActivity loginActivity);

    void inject(ProjectListFragment projectListFragment);

    void inject(ContactFragment contactFragment);

    void inject(EventFragment eventFragment);

    void inject(MemberlistFragment memberlistFragment);

    void inject(ConnectFragment connectFragment);

    void inject(NotificationActivity notificationActivity);

    void inject(HomeFragment homeFragment);

    void inject(NewsFragment newsFragment);

    void inject(AboutFragment aboutFragment);


}
