package com.dci.clupapp.retrofit;

/**
 * Created by harini on 12/5/2018.
 */

public class Fields {

    public static final class Login {
        public static final String MOBILENUMBER = "mobile_no";
        public static final String FCMKEY = "fcm_key";
        public static final String DEVICEID = "device_id";
        public static final String DEVICENAME = "device_name";
        public static final String DEVICEIME = "device_imei";
        public static final String DEVICEOS = "device_os";
        public static final String APPVERSION = "app_version";
    }
    public static final class CommonField {
        public static final String USERID = "user_id";
        public static final String FCMKEY = "fcm_key";
        public static final String DEVICEID = "device_id";
        public static final String DEVICEOS = "device_os";
        public static final String APPVERSION = "app_version";
        public static final String PAGE = "page";
        public static final String LATITUDE = "longitude";
        public static final String LONGITUDE = "latitude";
        public static final String PAGESIZE = "page_size";
        public static final String MESSAGE = "message";
        public static final String EMAIL = "user_email";
        public static final String NAME = "user_name";
    }
    public static final class Event {
        public static final String USERID = "user_id";
        public static final String DEVICEID = "device_id";
        public static final String FCMKEY = "fcm_key";
        public static final String DEVICEOS = "device_os";
        public static final String APPVERSION = "app_version";
        public static final String PAGE = "page";
        public static final String PAGESIZE = "page_size";

    }

    public static final class EventAccept {
        public static final String USERID = "user_id";
        public static final String EVENTID = "event_id";
        public static final String DEVICEID = "device_id";
        public static final String DEVICEOS = "device_os";
        public static final String FCMKEY = "fcm_key";
        public static final String APPVERSION = "app_version";

    }
}
