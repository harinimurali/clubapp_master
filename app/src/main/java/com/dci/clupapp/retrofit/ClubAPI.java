package com.dci.clupapp.retrofit;


import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.ConnectResponse;
import com.dci.clupapp.models.EventResponse;
import com.dci.clupapp.models.MemberResponse;
import com.dci.clupapp.models.NotificationResponse;
import com.dci.clupapp.models.ProfileResponse;
import com.dci.clupapp.models.ProjectResponse;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ClubAPI {
    @FormUrlEncoded
    @POST("login")
    Call<ProfileResponse> userLogin(@Field(Fields.Login.MOBILENUMBER) String mobilenum,
                                    @Field(Fields.Login.FCMKEY) String fcmkey,
                                    @Field(Fields.Login.DEVICEID) String deviceid,
                                    @Field(Fields.Login.DEVICENAME) String devicename,
                                    @Field(Fields.Login.DEVICEIME) String deviceime,
                                    @Field(Fields.Login.DEVICEOS) String deviceos,
                                    @Field(Fields.Login.APPVERSION) String appversion);

    @FormUrlEncoded
    @POST("userexist")
    Call<CommonResponse> userExist(@Field(Fields.Login.MOBILENUMBER) String mobilenum,
                                   @Field(Fields.Login.DEVICEID) String deviceid,
                                   @Field(Fields.Login.DEVICENAME) String devicename,
                                   @Field(Fields.Login.DEVICEIME) String deviceime,
                                   @Field(Fields.Login.DEVICEOS) String deviceos,
                                   @Field(Fields.Login.APPVERSION) String appversion);

    @FormUrlEncoded
    @POST("projects")
    Call<ProjectResponse> projectList(@Field(Fields.CommonField.USERID) String userid,
                                      @Field(Fields.CommonField.FCMKEY) String deviceid,
                                      @Field(Fields.CommonField.DEVICEID) String devicename,
                                      @Field(Fields.CommonField.DEVICEOS) String deviceos,
                                      @Field(Fields.CommonField.APPVERSION) String appversion,
                                      @Field(Fields.CommonField.PAGE) String page,
                                      @Field(Fields.CommonField.PAGESIZE) String pagesize);

    @FormUrlEncoded
    @POST("contact")
    Call<CommonResponse> ContactUs(@Field(Fields.CommonField.USERID) String userid,
                                   @Field(Fields.CommonField.FCMKEY) String deviceid,
                                   @Field(Fields.CommonField.DEVICEID) String devicename,
                                   @Field(Fields.CommonField.DEVICEOS) String deviceos,
                                   @Field(Fields.CommonField.NAME) String name,
                                   @Field(Fields.CommonField.EMAIL) String mail,
                                   @Field(Fields.CommonField.MESSAGE) String msg,
                                   @Field(Fields.CommonField.APPVERSION) String appversion);

    @FormUrlEncoded
    @POST("events")
    Call<EventResponse> eventList(@Field(Fields.Event.USERID) String userid,
                                  @Field(Fields.Event.DEVICEID) String deviceid,
                                  @Field(Fields.Event.FCMKEY) String fcmkey,
                                  @Field(Fields.Event.DEVICEOS) String deviceos,
                                  @Field(Fields.Event.APPVERSION) String appversion,
                                  @Field(Fields.Event.PAGE) String page,
                                  @Field(Fields.Event.PAGESIZE) String pagesize);

    @FormUrlEncoded
    @POST("acceptEvent")
    Call<CommonResponse> acceptList(@Field(Fields.EventAccept.USERID) String userid,
                                    @Field(Fields.EventAccept.EVENTID) String eventid,
                                    @Field(Fields.EventAccept.DEVICEID) String deviceid,
                                    @Field(Fields.EventAccept.DEVICEOS) String deviceos,
                                    @Field(Fields.EventAccept.FCMKEY) String fcmkey,
                                    @Field(Fields.EventAccept.APPVERSION) String appversion);

    @FormUrlEncoded
    @POST("members")
    Call<MemberResponse> memberList(@Field(Fields.CommonField.USERID) String userid,
                                    @Field(Fields.CommonField.FCMKEY) String deviceid,
                                    @Field(Fields.CommonField.DEVICEID) String devicename,
                                    @Field(Fields.CommonField.DEVICEOS) String deviceos,
                                    @Field(Fields.CommonField.APPVERSION) String appversion,
                                    @Field(Fields.CommonField.PAGE) String page,
                                    @Field(Fields.CommonField.PAGESIZE) String pagesize);

    @FormUrlEncoded
    @POST("nearby")
    Call<ConnectResponse> nearBy(@Field(Fields.CommonField.USERID) String userid,
                                 @Field(Fields.CommonField.FCMKEY) String deviceid,
                                 @Field(Fields.CommonField.DEVICEID) String devicename,
                                 @Field(Fields.CommonField.DEVICEOS) String deviceos,
                                 @Field(Fields.CommonField.APPVERSION) String appversion,
                                 @Field(Fields.CommonField.LATITUDE) String latitude,
                                 @Field(Fields.CommonField.LONGITUDE) String longitude);

    @FormUrlEncoded
    @POST("notifications")
    Call<NotificationResponse> notifyList(@Field(Fields.CommonField.USERID) String userid,
                                          @Field(Fields.CommonField.FCMKEY) String fcm_key,
                                          @Field(Fields.CommonField.DEVICEID) String device_id,
                                          @Field(Fields.CommonField.DEVICEOS) String device_os,
                                          @Field(Fields.CommonField.APPVERSION) String app_version,
                                          @Field(Fields.CommonField.PAGE) String page,
                                          @Field(Fields.CommonField.PAGESIZE) String page_size);

    @FormUrlEncoded
    @POST("trending")
    Call<JsonElement> trending(@Field(Fields.CommonField.USERID) String userid,
                               @Field(Fields.CommonField.FCMKEY) String fcm_key,
                               @Field(Fields.CommonField.DEVICEID) String device_id,
                               @Field(Fields.CommonField.DEVICEOS) String device_os,
                               @Field(Fields.Login.DEVICENAME) String devicename,
                               @Field(Fields.Login.DEVICEIME) String deviceime,
                               @Field(Fields.CommonField.APPVERSION) String app_version);

    /*@FormUrlEncoded
    @POST("news")
    Call<NewsResponse> newsList(@Field(Fields.CommonField.USERID) String userid,
                               @Field(Fields.CommonField.FCMKEY) String fcm_key,
                               @Field(Fields.CommonField.DEVICEID) String device_id,
                               @Field(Fields.CommonField.DEVICEOS) String device_os,
                               @Field(Fields.Login.DEVICENAME) String devicename,
                               @Field(Fields.Login.DEVICEIME) String deviceime,
                               @Field(Fields.CommonField.APPVERSION) String app_version);
    @FormUrlEncoded
    @POST("aboutus")
    Call<NewsResponse> aboutUs(@Field(Fields.CommonField.USERID) String userid,
                               @Field(Fields.CommonField.FCMKEY) String fcm_key,
                               @Field(Fields.CommonField.DEVICEID) String device_id,
                               @Field(Fields.CommonField.DEVICEOS) String device_os,
                               @Field(Fields.Login.DEVICENAME) String devicename,
                               @Field(Fields.Login.DEVICEIME) String deviceime,
                               @Field(Fields.CommonField.APPVERSION) String app_version);*/
}

