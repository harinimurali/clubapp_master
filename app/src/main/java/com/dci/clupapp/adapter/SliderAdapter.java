package com.dci.clupapp.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.BannerObjects;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by harini on 12/4/2018.
 */

public class SliderAdapter extends PagerAdapter {


    private ArrayList<BannerObjects> imageModelArrayList;
    private LayoutInflater inflater;
    private Context context;


    public SliderAdapter(Context context, ArrayList<BannerObjects> imageModelArrayList) {
        this.context = context;
        this.imageModelArrayList = imageModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imageModelArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.banner_item_view, view, false);

        assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.image);
        final TextView title = imageLayout.findViewById(R.id.title);
        final TextView type = imageLayout.findViewById(R.id.location);


        Picasso.get().load(Constants.ImageUrl + imageModelArrayList.get(position).getImage().trim()).into(imageView);
        title.setText(imageModelArrayList.get(position).getTitle());
        type.setText(imageModelArrayList.get(position).getType());
        // imageView.setImageResource(Integer.parseInt(imageModelArrayList.get(position).getImage()));

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}