package com.dci.firebaseexample;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;



import java.util.List;


public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder>  {

    private List<User> users;
    private Context context;
    private OnItemClickListener listener;

    public ContactListAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }


    @NonNull
    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_posttime, parent, false);

        return new ContactListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactListAdapter.ViewHolder viewHolder, final int position) {
        User user = users.get(position);
        viewHolder.text_postname.setText(user.getName());
        viewHolder.relative_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClick(position,view);
            }
        });


    }

    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text_postname;
        public RelativeLayout relative_parent;

        public ViewHolder(View view) {
            super(view);
            text_postname = view.findViewById(R.id.text_username);
            relative_parent = view.findViewById(R.id.relative_parent);

        }
    }
}
