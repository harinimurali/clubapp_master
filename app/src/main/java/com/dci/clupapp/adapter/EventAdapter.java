package com.dci.clupapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.BaseActivity;
import com.dci.clupapp.activity.EventDetailActivity;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.fragment.EventFragment;
import com.dci.clupapp.models.EventDTO;
import com.dci.clupapp.models.EventResponse;

import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by keerthana on 11/8/2018.
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {


    private List<EventResponse.EventResults> eventList;
    private Context context;
    private AddTouchListen addTouchListen;


    public EventAdapter(List<EventResponse.EventResults> eventList) {
        this.context = context;

        this.eventList = eventList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private Button rsvp_btn;
        public TextView title;
        ImageView img, menu;
        public TextView desc, date, time;
        public TextView location_txt;
        public LinearLayout lin;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.description);
            location_txt = (TextView) view.findViewById(R.id.location_txt);
            img = (ImageView) view.findViewById(R.id.image);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            lin = (LinearLayout) view.findViewById(R.id.lin);
            rsvp_btn = (Button) view.findViewById(R.id.rsvp_btn);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.events_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final EventResponse.EventResults movie = eventList.get(position);
        holder.title.setText(movie.getData().get(position).getName());
        holder.desc.setText(movie.getData().get(position).getDescription());
        String date = BaseFragment.getDateFormat(movie.getData().get(position).getUpdatedAt());
        holder.date.setText(date);


        String time = BaseFragment.getTimeFormat(movie.getData().get(position).getUpdatedAt());
        holder.time.setText(time);
        Picasso.get().load(Constants.ImageUrl + movie.getData().get(position).getImage()).into(holder.img);
        if (movie.getData().get(position).getStatus() == 1) {
            holder.rsvp_btn.setVisibility(View.GONE);

        } else {
            holder.rsvp_btn.setVisibility(View.VISIBLE);
        }
        holder.location_txt.setText(movie.getData().get(position).getVendor().getAddress2());


        holder.rsvp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchAcceptClick(position);
                }

            }

        });

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);

        public void onTouchAcceptClick(int position);
    }
}