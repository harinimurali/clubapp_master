package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.GalleryDTO;
import com.dci.clupapp.models.GalleryDetailDTO;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 11/16/2018.
 */


public class GalleryDetailAdapter extends RecyclerView.Adapter<GalleryDetailAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
    private List<GalleryDetailDTO> galList;
    private Context context;
    private AddTouchListen addTouchListen;

    public GalleryDetailAdapter(List<GalleryDetailDTO> galList) {
        this.galList = galList;
        this.context =context;
    }


    //    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout header;
        SelectableRoundedImageView img;
        public VideoView v;


        public MyViewHolder(View view) {
            super(view);
            img=(SelectableRoundedImageView)view.findViewById(R.id.image);
            header=(LinearLayout)view.findViewById(R.id.header);
          /*  videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
            videoLayout.setActivity((Activity) context);
*/
        }
    }


    public GalleryDetailAdapter(List<GalleryDetailDTO> galList,Context context) {
        this.galList = galList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        GalleryDetailDTO movie = galList.get(position);

        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
      /*  Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        holder.v.setVideoURI(videoUri);*/

        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return galList.size();
    }
}
