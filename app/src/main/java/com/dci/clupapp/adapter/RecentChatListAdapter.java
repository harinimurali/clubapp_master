package com.dci.firebaseexample;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

public class RecentChatListAdapter extends BaseAdapter {

    public RecentChatListAdapter(Context context, List<ChatMessageParams> contactLists) {
        this.context = context;
        this.contactLists = contactLists;
    }

    Context context;
    List<ChatMessageParams> contactLists;


    @Override
    public int getCount() {
        return contactLists.size();
    }

    @Override
    public ChatMessageParams getItem(int position) {
        return contactLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_contact_list, null);
        }

        TextView textName = (TextView) convertView.findViewById(R.id.text_name);
        TextView text_designation = (TextView) convertView.findViewById(R.id.text_designation);

            textName.setText(contactLists.get(position).getToName());
            if (contactLists.get(position).getText().equals("image")){
                text_designation.setText("image");
            }
            else {
                text_designation.setText(contactLists.get(position).getText());
            }



        return convertView;
    }


}
