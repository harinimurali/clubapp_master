package com.dci.clupapp.adapter;

import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.VideoDTO;
import com.dci.clupapp.models.VideoResponse;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 11/19/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    private  TextView text;
    // private  FullscreenVideoLayout videoLayout;
    private List<VideoResponse.VideoResults.VideoDataItem> videoList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;



    private String url="http://clubapp.dci.in/uploads/video/SampleVideo_720x480_1mb.mp4";

    public VideoAdapter(List<VideoResponse.VideoResults.VideoDataItem> videoRespons) {
        this.videoList = videoRespons;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView views,status,time;
        private  FullscreenVideoLayout videoView;
        private LinearLayout header;
        ImageView img;
        public VideoView v;
        public TextView text,date;
        //   public JzvdStd jzvdStd;


        public MyViewHolder(View view) {
            super(view);
            // video=(FullscreenVideoLayout)view.findViewById(R.id.videoView);

            date=view.findViewById(R.id.date);
            time=view.findViewById(R.id.time);
            text=view.findViewById(R.id.text);
            videoView = (FullscreenVideoLayout) view.findViewById(R.id.videoView);

        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_view_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final VideoResponse.VideoResults.VideoDataItem movie = videoList.get(position);
        Uri videoUri = Uri.parse(movie.getFilePath());
        try {
            holder.videoView.setVideoURI(videoUri);
            holder.videoView.pause();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // holder.jzvdStd.setUp("http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4"
        //, "饺子闭眼睛" , Jzvd.SCREEN_WINDOW_NORMAL);
        // holder.jzvdStd.thumbImageView.setImage("http://p.qpic.cn/videoyun/0/2449_43b6f696980311e59ed467f22794e792_1/640");


        holder.text.setText(movie.getTitle());
        holder.date.setText(movie.getUpdatedAt());
        String date=BaseFragment.getDateFormat(movie.getUpdatedAt());
        holder.date.setText(date);
        holder.time.setText(movie.getUpdatedAt());
        String time= BaseFragment.getTimeFormat(movie.getUpdatedAt());
        holder.time.setText(time);
        // holder.text1.setText(movie.getText1());
        //  holder.img.setImageResource(Integer.parseInt(movie.getImg()));
      /*  Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        holder.v.setVideoURI(videoUri);*/


    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return videoList.size();
    }
}

