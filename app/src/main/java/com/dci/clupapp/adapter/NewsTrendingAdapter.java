package com.dci.clupapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.NewsTrendingDTO;
import com.dci.clupapp.utils.AddTouchListen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keerthana on 12/3/2018.
 */

public class NewsTrendingAdapter extends RecyclerView.Adapter<NewsTrendingAdapter.MyViewHolder> {


    private List<NewsTrendingDTO> newsList;
    public AddTouchListen addTouchListen;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;


        public MyViewHolder(View view) {
            super(view);

            //binding view
            ButterKnife.bind(this, view);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public NewsTrendingAdapter(List<NewsTrendingDTO> newsList) {
        this.newsList = newsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_trending_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        NewsTrendingDTO list = newsList.get(position);
        holder.title.setText(list.getTitle());
//        holder.image.setImageResource(Integer.parseInt(list.getImage()));


       /* holder.mlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}