package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.ConnectResponse;
import com.dci.clupapp.models.MembersDTO;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by harini on 11/20/2018.
 */

public class ConnectListAdapter extends RecyclerView.Adapter<ConnectListAdapter.MyViewHolder> {


    private List<ConnectResponse.ResultsItem> memberlist;
    public AddTouchListener addTouchListen;
    Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.member_name)
        public TextView member_name;
        @BindView(R.id.member_position)
        TextView member_position;
        @BindView(R.id.connect_km)
        TextView connectKm;
        @BindView(R.id.member_profileimage)
        ImageView member_profileimage;
        @BindView(R.id.chat)
        ImageView memberChat;
        @BindView(R.id.call)
        ImageView memberCall;
        @BindView(R.id.relative_click)
        RelativeLayout relative_click;

        public MyViewHolder(View view) {
            super(view);
            //binding view
            ButterKnife.bind(this, view);
        }
    }

    public interface AddTouchListener {
        public void onTouchClick(int position);

        public void onTouchCall(int position);

        public void onTouchChat(int position);

    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    public ConnectListAdapter(List<ConnectResponse.ResultsItem> memberlist, Context context) {
        this.memberlist = memberlist;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.connect_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ConnectResponse.ResultsItem movie = memberlist.get(position);
        holder.member_name.setText(movie.getFirstname());
        holder.member_position.setText(movie.getPositionHeld());
        holder.connectKm.setText(movie.getAddress1());
        Picasso.get().load(Constants.ImageUrl + movie.getProfileImage()).into(holder.member_profileimage);

        holder.relative_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
        holder.memberCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchCall(position);
                }
            }
        });
        holder.memberChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchChat(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return memberlist.size();
    }
}