package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.NotificationResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by keerthana on 12/12/2018.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
    private List<NotificationResponse.NotificationResults> notiList;
    private Context context;
    private AddTouchListen addTouchListen;

    public NotificationsAdapter(List<NotificationResponse.NotificationResults> notiList) {
        this.context=context;

        this.notiList=notiList;
    }




    //    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rel;
        ImageView img;
        public TextView time,message,title;


        public MyViewHolder(View view) {
            super(view);
            img=(ImageView)view.findViewById(R.id.profile);
            rel=(RelativeLayout)view.findViewById(R.id.rel);
            time=(TextView)view.findViewById(R.id.time);
            message=(TextView)view.findViewById(R.id.message);
            title=(TextView)view.findViewById(R.id.title);

        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifications_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final NotificationResponse.NotificationResults movie = notiList.get(position);
        holder.title.setText(movie.getData().get(position).getUsers().getFirstname());
        holder.message.setText(movie.getData().get(position).getMessage());
        holder.time.setText(movie.getData().get(position).getUpdatedAt());
        String time=  BaseFragment.getTimeFormat(movie.getData().get(position).getUpdatedAt());
        holder.time.setText(time);




      //  holder.img.setImageResource(Integer.parseInt(movie.getImage()));
      /*  Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        holder.v.setVideoURI(videoUri);*/

        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return notiList.size();
    }
}
