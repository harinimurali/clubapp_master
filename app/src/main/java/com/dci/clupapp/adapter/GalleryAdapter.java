package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.GalleryDTO;
import com.dci.clupapp.models.GalleryResponse;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 11/16/2018.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private List<GalleryResponse.GalleryResult> galleryList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView event,count;
        private ImageView profile;
        private RelativeLayout rel;
        public TextView title;
        ImageView img,menu;
        public TextView desc,date,time;
        public TextView location_txt;

        public MyViewHolder(View view) {
            super(view);
            profile=view.findViewById(R.id.profile);
            event=view.findViewById(R.id.event);
            count=view.findViewById(R.id.album_count);
            time=view.findViewById(R.id.time);
            rel=view.findViewById(R.id.rel);

        }
    }


    public GalleryAdapter(List<GalleryResponse.GalleryResult> galleryList) {
        this.galleryList = galleryList;
        this.context =context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final GalleryResponse.GalleryResult movie = galleryList.get(position);
        holder.event.setText(movie.getName());
        holder.count.setText(movie.getDescription());
        holder.time.setText(movie.getUpdatedAt());
        Log.e("image", "description" + Constants.ImageUrl + movie.getAttachment().get(0).getFilePath());

        Picasso.get().load(Constants.ImageUrl + movie.getAttachment().get(0).getFilePath()).into(holder.profile);


        String time= BaseFragment.getTimeFormat(movie.getUpdatedAt());
        holder.time.setText(time);
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }
    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}
