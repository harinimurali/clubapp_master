package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.ProfileResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtpVerificationFragment extends BaseFragment {

    @BindView(R.id.otp_verify)
    ImageView otpVerify;


    private ImageView otp_verify;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    ProfileResponse userProfileObj;

    public OtpVerificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_otp_verification, container, false);
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getContext().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.DEVICEOS, Build.VERSION.SDK_INT).apply();
        editor.putInt(Constants.OSVERSION, Build.VERSION.SDK_INT).apply();
        otp_verify = view.findViewById(R.id.otp_verify);

        otp_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpVerifyAPI();
            }
        });

        return view;
    }

    public void otpVerifyAPI() {
        showProgress();
        String appid = sharedPreferences.getString(Constants.APPID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String mobilenumber = sharedPreferences.getString(Constants.MOBILENUMBER, "");
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.userLogin("9487292025", fcm_key, deviceid, "android", osversion, osversion, appversion).
       // clubAPI.userLogin("9487292025", "ygjhtdryhdfghdfhdghgfhf", "8568685", "android", "7574574575", "24", "1.0").
                enqueue(new Callback<ProfileResponse>() {
                    @Override
                    public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                        try {
                            if (response.body() != null) {
                                userProfileObj = response.body();
                                hideProgress();
                                Log.e("response", ">>" + userProfileObj.getStatus());
                                if (userProfileObj.getStatus().equals("Success")) {
                                    List<ProfileResponse> userProfileObjs = new ArrayList<>();
                                    userProfileObjs.add(userProfileObj);
                                    AppPreferences.setProfile(getActivity(), userProfileObjs);
                                    editor.putString(Constants.USERID, String.valueOf(userProfileObj.getResults().getId()));
                                    editor.putString(Constants.VENDORID, String.valueOf(userProfileObj.getResults().getVendorId()));
                                    editor.putBoolean(Constants.LOGIN_STATUS, true).commit();

                                   AppPreferences.setLoginStatus(getActivity(),true);
                                    // editor.putString(Constants.SECURITYPIN, userProfileObj.getResults());
                                    editor.commit();
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                } else {
                                    hideProgress();
                                    AppPreferences.setLoginStatus(getActivity(),false);
                                    editor.putBoolean(Constants.LOGIN_STATUS, false);
                                    Toast.makeText(getActivity(), "Try again..", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                hideProgress();
                                AppPreferences.setLoginStatus(getActivity(),false);
                                editor.putBoolean(Constants.LOGIN_STATUS, false);
                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            hideProgress();
                            AppPreferences.setLoginStatus(getActivity(),false);
                            editor.putBoolean(Constants.LOGIN_STATUS, false);
                            Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });

    }

}
