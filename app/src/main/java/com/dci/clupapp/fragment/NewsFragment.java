package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.adapter.NewsTrendingAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.NewsTrendingDTO;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    private NewsTrendingAdapter newsListAdapter;
    private List<NewsTrendingDTO> newsList=new ArrayList<>();
    private RecyclerView newsListRecycle;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_news, container, false);


        newsListRecycle=view.findViewById(R.id.news_recyclerview);
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        NewsData();
        newsListAdapter = new NewsTrendingAdapter(newsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        newsListRecycle.setLayoutManager(mLayoutManager);
        newsListRecycle.setItemAnimator(new DefaultItemAnimator());
        newsListRecycle.setAdapter(newsListAdapter);


        viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);



        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        //   GalleryFragment.PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        adapter.addFragment(new NewsTodayFragment(), "Today's News");
        adapter.addFragment(new NewsMonthFragment(), "This Month");
        viewPager.setAdapter(adapter);


    }
    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void NewsData() {
        NewsTrendingDTO s=new NewsTrendingDTO();
      //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);

        s=new NewsTrendingDTO();
      //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);

        s=new NewsTrendingDTO();
      //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);

        s=new NewsTrendingDTO();
      //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);
    }
    public void newsAPI() {
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String appversion = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));

/*
        clubAPI.newsList(userid,fcm_key, deviceid, deviceos, "android",deviceid, appversion)
                .enqueue(new Callback<NewsResponse>() {
                    @Override
                    public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {

                        try {
                            //  hideProgress();
                            if (response.body() != null) {
                                Log.e("response", ">>" + response.body());
                                if (commonResponse.getStatus().equals("Success")) {
                                    Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();

                                } else if (commonResponse.getStatus().equals("fail")) {
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            startActivity(new Intent(getActivity(), MainActivity.class));
                        }

                    }


                    @Override
                    public void onFailure(Call<NewsResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });
*/

    }

}
