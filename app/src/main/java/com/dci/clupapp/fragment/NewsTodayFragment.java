package com.dci.clupapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dci.clupapp.R;
import com.dci.clupapp.adapter.NewsTodayAdapter;
import com.dci.clupapp.adapter.NewsTrendingAdapter;
import com.dci.clupapp.models.NewsTodayDTO;
import com.dci.clupapp.models.NewsTrendingDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsTodayFragment extends Fragment {


    private RecyclerView newsListRecycle;
    private NewsTodayAdapter newsListAdapter;
    private List<NewsTodayDTO> newsList=new ArrayList<>();


    public NewsTodayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_news_today, container, false);



        newsListRecycle=view.findViewById(R.id.news_today_recyclerview);

        NewsData();
        newsListAdapter = new NewsTodayAdapter(newsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        newsListRecycle.setLayoutManager(mLayoutManager);
        newsListRecycle.setItemAnimator(new DefaultItemAnimator());
        newsListRecycle.setAdapter(newsListAdapter);

        return view;
    }

    private void NewsData() {
        NewsTodayDTO s=new NewsTodayDTO();
        s.setCategory("Business");
        s.setDesc("At vero eos at accumus at isto ud dig duciumus");
        s.setDuration("02:30 PM");
        s.setName("Javin Chrome");
        s.setImage(String.valueOf(R.mipmap.party));
        newsList.add(s);

        s=new NewsTodayDTO();
        s.setCategory("Business");
        s.setDesc("At vero eos at accumus at isto ud dig duciumus");
        s.setDuration("02:30 PM");
        s.setName("Javin Chrome");
        s.setImage(String.valueOf(R.mipmap.funimage));
        newsList.add(s);


        s=new NewsTodayDTO();
        s.setCategory("Business");
        s.setDesc("At vero eos at accumus at isto ud dig duciumus");
        s.setDuration("02:30 PM");
        s.setName("Javin Chrome");
        s.setImage(String.valueOf(R.mipmap.party));
        newsList.add(s);

        s=new NewsTodayDTO();
        s.setCategory("Business");
        s.setDesc("At vero eos at accumus at isto ud dig duciumus");
        s.setDuration("02:30 PM");
        s.setName("Javin Chrome");
        s.setImage(String.valueOf(R.mipmap.funimage));
        newsList.add(s);
    }

}
