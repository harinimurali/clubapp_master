package com.dci.clupapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

public class EventDetailActivity extends BaseActivity {

    private ImageView back, mImage;
    private TextView mTitle, mDesc, mDuration, mLocation;

    String name, description, date, location, image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        back = findViewById(R.id.back);
        mImage = findViewById(R.id.image);
        mTitle = findViewById(R.id.mTitle);
        mDesc = findViewById(R.id.mDesc);
        mDuration = findViewById(R.id.mDuration);
        mLocation = findViewById(R.id.mLocation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("type");
        name = bundle.getString("name");
        description = bundle.getString("description");
        date = bundle.getString("date");
        location = bundle.getString("location");
        image = bundle.getString("image");

        mTitle.setText(name);
        mDesc.setText(description);
        mDuration.setText(date);
        mLocation.setText(location);
        Picasso.get().load(Constants.ImageUrl + image).into(mImage);


    }
}
