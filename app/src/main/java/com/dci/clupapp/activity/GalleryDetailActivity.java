package com.dci.clupapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dci.clupapp.R;
import com.dci.clupapp.adapter.GalleryAdapter;
import com.dci.clupapp.adapter.GalleryDetailAdapter;
import com.dci.clupapp.models.GalleryDetailDTO;
import com.dci.clupapp.models.GalleryResponse;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryDetailActivity extends BaseActivity {
    @BindView(R.id.gallery_recycleview)
    RecyclerView gallery_recycleview;
    @BindView(R.id.back)
    ImageView back_arrow;
    private RecyclerView recyclerview;
    private List<GalleryResponse> galList ;
    private GalleryDetailAdapter mAdapter;

    String image,name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

       Intent i = getIntent();
       i.getParcelableArrayListExtra("data");
       
//        Bundle bundle = i.getBundleExtra("data");
//        image=bundle.getString("image");


      /*  mTitle.setText(name);
        mDesc.setText(description);
        mDuration.setText(time);*/

        mAdapter = new GalleryDetailAdapter(galList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        gallery_recycleview.setLayoutManager(gridLayoutManager);
        gallery_recycleview.setItemAnimator(new DefaultItemAnimator());

        Intent intent=new Intent();
       /* intent.setData(Uri.parse(galList.get(0).getImg()));
        intent.putExtra("data",galList.get(0).getImg());*/

        ArrayList<GalleryResponse.GalleryResult.GalleryAttachment> anyObjectListReceive = (ArrayList<GalleryResponse.GalleryResult.GalleryAttachment>) getIntent().getExtras().getSerializable("KEY_VALUE");
        Log.d("", "onCreate: "+getIntent().getExtras());
       // anyObjectListReceive = (ArrayList<GalleryResponse.GalleryResult.GalleryAttachment>) getIntent().getSerializableExtra("Player1");
        //   image= galList.get(0).getImg();
        gallery_recycleview.setAdapter(mAdapter);
    }


    @OnClick(R.id.back)
    public void onButtonClick(View view) {
        onBackPressed();
    }
/*
    private void AnalData() {
        GalleryDetailDTO s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.test));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.test));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

    }
*/
}
