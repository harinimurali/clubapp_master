package com.dci.clupapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.adapter.EventAdapter;
import com.dci.clupapp.adapter.NotificationsAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.EventResponse;
import com.dci.clupapp.models.NotificationResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {


    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    @Inject
    public ClubAPI clubAPI;
    private RecyclerView notificationrecyclerview;
    private NotificationsAdapter notificationsAdapter;
    private List<NotificationResponse.NotificationResults> notiList;
    public NotificationResponse notificationResponse;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        back=findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();


        fcmSharedPrefrences = getApplicationContext().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getApplicationContext().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.OSVERSION, Build.VERSION.SDK_INT).apply();
        notificationrecyclerview=findViewById(R.id.notification_recyclerview);

        NotificationsAPI();

//        notiList=new ArrayList<>();
//
//        mAdapter = new NotificationsAdapter(notiList);
//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
//
//        notificationrecyclerview.setLayoutManager(mLayoutManager);
//
//        notificationrecyclerview.setItemAnimator(new DefaultItemAnimator());
//
//        notificationrecyclerview.setAdapter(mAdapter);



    }

    private void NotificationsAPI() {

        String userid = sharedPreferences.getString(Constants.USERID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String page = String.valueOf(sharedPreferences.getInt(Constants.PAGE, 0));
        String pagesize = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.notifyList("1",deviceid,fcm_key,deviceos,appversion,"1","100").
                enqueue(new Callback<NotificationResponse>() {


            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                try {
                    //  hideProgress();
                    if (response.body() != null) {
                        notificationResponse = response.body();
                        Log.e("response", ">>" + response.body());
                        if (notificationResponse.getStatus().equals("Success")) {

                            final List<NotificationResponse.NotificationResults> notificationResponse = new ArrayList<>();
                            notificationResponse.add(response.body().getResults());

                            notificationsAdapter = new NotificationsAdapter(notificationResponse);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

                            notificationrecyclerview.setLayoutManager(mLayoutManager);

                            notificationrecyclerview.setItemAnimator(new DefaultItemAnimator());

                            notificationrecyclerview.setAdapter(notificationsAdapter);


                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                  //  startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                }

            }


            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                //  hideProgress();
                Toast.makeText(NotificationActivity.this,getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
            }
        });


    }

/*
    private void NotificationData() {
        NotifiactionsDTO s=new NotifiactionsDTO();
        s.setImage(String.valueOf(R.mipmap.profile));
        s.setMessage("Hi how are you");
        s.setTime("10:30 Am");
        s.setTitle("Business Meeting");
        notiList.add(s);

        s=new NotifiactionsDTO();
        s.setImage(String.valueOf(R.mipmap.profile));
        s.setMessage("Hi how are you");
        s.setTime("10:30 Am");
        s.setTitle("Business Meeting");
        notiList.add(s);

        s=new NotifiactionsDTO();
        s.setImage(String.valueOf(R.mipmap.profile));
        s.setMessage("Hi how are you");
        s.setTime("10:30 Am");
        s.setTitle("Business Meeting");
        notiList.add(s);

        s=new NotifiactionsDTO();
        s.setImage(String.valueOf(R.mipmap.profile));
        s.setMessage("Hi how are you");
        s.setTime("10:30 Am");
        s.setTitle("Business Meeting");
        notiList.add(s);
    }
*/
}
