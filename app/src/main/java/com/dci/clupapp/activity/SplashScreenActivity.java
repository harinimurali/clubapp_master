package com.dci.clupapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.dci.clupapp.R;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;

import javax.inject.Inject;

/**
 * Created by harini on 12/3/2018.
 */

public class SplashScreenActivity extends BaseActivity implements Animation.AnimationListener {
    @Inject
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        FrameLayout mainFrame = ((FrameLayout) findViewById(R.id.splash_frame));
        Animation zoom_out = AnimationUtils.loadAnimation(this,
                R.anim.zoom_out);
        zoom_out.setAnimationListener(this);
        mainFrame.startAnimation(zoom_out);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            //AppPreferences.setversioncode(getApplicationContext(), String.valueOf(verCode));

            Log.e("payment", "version" + version + "-" + verCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Intent i;
        Log.e("logged","/"+AppPreferences.isLoggedIn(SplashScreenActivity.this));
        if (AppPreferences.isLoggedIn(SplashScreenActivity.this)) {
            i = new Intent(SplashScreenActivity.this, MainActivity.class);
        } else {
            i = new Intent(SplashScreenActivity.this, LoginActivity.class);
        }
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(i);
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

