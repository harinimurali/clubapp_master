package com.dci.clupapp.activity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.models.MemberResponse;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harini on 11/9/2018.
 */

public class MemberDetailActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back_arrow;
    @BindView(R.id.profile_image)
    ImageView profileImage;
    @BindView(R.id.member_name)
    TextView mName;
    @BindView(R.id.member_mobile)
    TextView mmobile;
    @BindView(R.id.member_email)
    TextView mEmail;
    @BindView(R.id.member_dob)
    TextView mDob;
    @BindView(R.id.member_address)
    TextView mAddress;
    @BindView(R.id.member_classification)
    TextView mClassification;
    @BindView(R.id.member_position)
    TextView mPosition;

    ArrayList<MemberResponse.memberData> object;
    MemberResponse.memberData memberData;
    String name, id, position, classification, dob, city, address, mobilenum, email, image;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);
        ButterKnife.bind(this);
        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("data");
        name = bundle.getString("name");
        id = bundle.getString("id");
        position = bundle.getString("position");
        classification = bundle.getString("classification");
        dob = bundle.getString("dob");
        city = bundle.getString("city");
        address = bundle.getString("address");
        mobilenum = bundle.getString("mobile");
        email = bundle.getString("email");
        image = bundle.getString("image");

        mName.setText(name);
        mmobile.setText(mobilenum);
        mPosition.setText(position);
        mClassification.setText(classification);
        mAddress.setText(address + ", " + city);
        mEmail.setText(email);
        mDob.setText(dob);
        Picasso.get().load(Constants.ImageUrl + image).into(profileImage);


        // memberData= (MemberResponse.memberData) i.getSerializableExtra("data");

    }


    @OnClick(R.id.back)
    public void onButtonClick(View view) {
        onBackPressed();
    }
}
