package com.dci.clupapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harini on 11/19/2018.
 */

public class ProjectDetailActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back_arrow;
    @BindView(R.id.project_title)
    TextView mTitle;
    @BindView(R.id.project_location)
    TextView mLocation;
    @BindView(R.id.date)
    TextView mDate;
    @BindView(R.id.project_description)
    TextView mDescription;
    @BindView(R.id.project_image)
    ImageView mImage;

    String name, eventid, image, description, venue, date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        ButterKnife.bind(this);
        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("data");
        name = bundle.getString("name");
        eventid = bundle.getString("eventid");
        description = bundle.getString("description");
        venue = bundle.getString("location");
        date = bundle.getString("date");
        image = bundle.getString("image");

        mTitle.setText(name);
        mDescription.setText(description);
        mDate.setText(getDateFormat(date));
        mLocation.setText(venue);
        Picasso.get().load(Constants.ImageUrl + image).into(mImage);

    }

    @OnClick(R.id.back)
    public void onButtonClick(View view) {
        onBackPressed();
    }
}
