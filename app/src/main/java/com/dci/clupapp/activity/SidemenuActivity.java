package com.dci.clupapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.adapter.DrawerItemCustomAdapter;
import com.dci.clupapp.fragment.EventFragment;
import com.dci.clupapp.fragment.MemberlistFragment;
import com.dci.clupapp.models.DataDTO;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

/**
 * Created by harini on 11/8/2018.
 */

public class SidemenuActivity extends AppCompatActivity {
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private ListView mDrawerList;
    ImageView close, profile_image;
    TextView username;
    private int position;
    private RelativeLayout rel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sidemenu);
        mTitle = mDrawerTitle = getTitle();
        mDrawerList = (ListView) findViewById(R.id.list_view);
        close = (ImageView) findViewById(R.id.close);
        profile_image = findViewById(R.id.user_profile);
        username = findViewById(R.id.user_name);
        rel = (RelativeLayout) findViewById(R.id.rel);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        DataDTO[] drawerItem = new DataDTO[12];

        drawerItem[0] = new DataDTO("Member Directory");
        drawerItem[1] = new DataDTO("Events Calendar");
        drawerItem[2] = new DataDTO("Contact us");
        drawerItem[3] = new DataDTO("About");
        drawerItem[4] = new DataDTO("Gallery");
        drawerItem[5] = new DataDTO("Videos");
        drawerItem[6] = new DataDTO("Connect");
        drawerItem[7] = new DataDTO("Projects");
        drawerItem[8] = new DataDTO("News");
        drawerItem[9] = new DataDTO("Music");
        drawerItem[10] = new DataDTO("Website Blog Link");
        drawerItem[11] = new DataDTO("Facebook/Twitter Feedback");

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        try {
            username.setText(AppPreferences.getProfile(SidemenuActivity.this).get(0).getResults().getFirstname());
            Picasso.get().load(Constants.ImageUrl + AppPreferences.getProfile(SidemenuActivity.this).get(0).getResults().getProfileImage()).placeholder(R.drawable.ic_user).into(profile_image);
        } catch (Exception e) {
            Picasso.get().load(R.drawable.ic_user).into(profile_image);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                Intent member = new Intent();
                // TODO Add extras or a data URI to this intent as appropriate.
                member.putExtra("type", "member");
                setResult(Activity.RESULT_OK, member);
                finish();
                break;
            case 1:

                Intent resultIntent = new Intent();
                // TODO Add extras or a data URI to this intent as appropriate.
                resultIntent.putExtra("type", "event");
                setResult(Activity.RESULT_OK, resultIntent);
                finish();

                break;

            case 2:
                Intent contact = new Intent();
                contact.putExtra("type", "contactus");
                setResult(Activity.RESULT_OK, contact);
                finish();
                //fragment=new ContactFragment();
                break;

            case 3:

                Intent about = new Intent();
                about.putExtra("type", "about");
                setResult(Activity.RESULT_OK, about);
                finish();
                break;

            case 4:

                Intent gallery = new Intent();
                gallery.putExtra("type", "gallery");
                setResult(Activity.RESULT_OK, gallery);
                finish();


                //   fragment =new GalleryFragment();
                break;

            case 5:

                Intent video = new Intent();
                video.putExtra("type", "video");
                setResult(Activity.RESULT_OK, video);
                finish();
                break;

            case 6:
                Intent connect = new Intent();
                connect.putExtra("type", "connect");
                setResult(Activity.RESULT_OK, connect);
                finish();
                break;

            case 7:
                Intent project = new Intent();
                project.putExtra("type", "projects");
                setResult(Activity.RESULT_OK, project);
                finish();

                //    fragment =new AboutFragment();
                break;

            case 8:

                Intent news = new Intent();
                news.putExtra("type", "news");
                setResult(Activity.RESULT_OK, news);
                finish();
                break;

            case 9:
                Intent music = new Intent();
                music.putExtra("type", "music");
                setResult(Activity.RESULT_OK, music);
                finish();
                break;
            case 10:

                break;
            case 11:
                Intent post = new Intent();
                post.putExtra("type", "post");
                setResult(Activity.RESULT_OK, post);
                finish();
                break;
            default:
                fragment = new EventFragment();
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.main, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

   /* private void replaceframent(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main, fragment).commit();

        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
    }*/

}
