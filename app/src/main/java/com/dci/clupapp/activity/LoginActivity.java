package com.dci.clupapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.fragment.OtpVerificationFragment;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.ProfileResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.mobile_number_edit)
    EditText mobileNumberEditText;
    @BindView(R.id.send)
    ImageView send;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;

    CommonResponse commonResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        send = findViewById(R.id.send);
        mobileNumberEditText = findViewById(R.id.mobile_number_edit);
        ClubApplication.getContext().getComponent().inject(this);

        editor = sharedPreferences.edit();
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.DEVICEOS, Build.VERSION.SDK_INT).apply();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobileNumberEditText.getText().toString().isEmpty() || mobileNumberEditText.getText().toString().length() != 10) {
                    mobileNumberEditText.setError(getResources().getString(R.string.field_required));
                } else {
                    numberVerifyAPI();
                    mobileNumberEditText.setError(null);
                }
            }
        });
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    public void numberVerifyAPI() {
        showProgress();
        String appid = sharedPreferences.getString(Constants.APPID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String mobilenumber = mobileNumberEditText.getText().toString();

        clubAPI.userExist("9487292025", deviceid, "android", appid, osversion, appversion).
                enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        try {
                            hideProgress();
                            if (response.body() != null) {
                                commonResponse = response.body();
                                if (commonResponse.getStatus().equalsIgnoreCase("Success")) {
                                    editor.putString(Constants.MOBILENUMBER, mobileNumberEditText.getText().toString());
                                    Fragment fragment = new OtpVerificationFragment();
                                    replaceFragment(fragment);
                                } else {
                                    Toast.makeText(LoginActivity.this, commonResponse.getMessage().toString(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                    }
                });

    }


}
