package com.dci.clupapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.dci.clupapp.R;

import com.dci.clupapp.fragment.AboutFragment;
import com.dci.clupapp.fragment.ConnectFragment;
import com.dci.clupapp.fragment.ContactFragment;
import com.dci.clupapp.fragment.EventFragment;
import com.dci.clupapp.fragment.GalleryFragment;
import com.dci.clupapp.fragment.HomeFragment;
import com.dci.clupapp.fragment.MemberlistFragment;
import com.dci.clupapp.fragment.MusicPlayerFragment;
import com.dci.clupapp.fragment.NewsFragment;
import com.dci.clupapp.fragment.PostFragment;
import com.dci.clupapp.fragment.ProjectListFragment;
import com.dci.clupapp.fragment.VideoFragment;
import com.github.fabtransitionactivity.SheetLayout;
import com.sergiocasero.revealfab.RevealFAB;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements SheetLayout.OnFabAnimationEndListener, AHBottomNavigation.OnTabSelectedListener {
    private RevealFAB revealFAB;
    @BindView(R.id.bottom_sheet)
    SheetLayout mSheetLayout;
    @BindView(R.id.menu)
    ImageView menu;
  //  @BindView(R.id.title)
    public static TextView title;
    @BindView(R.id.bottom_navigation)
    AHBottomNavigation ahBottomNavigation;
    @BindView(R.id.toolbar_actionbar)
    Toolbar toolbar;
    Fragment fragment;
    @BindView(R.id.notification)
    ImageView notification;

    private static final int REQUEST_CODE = 1;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // revealFAB =  findViewById(R.id.reveal_fab);
        //     mSheetLayout = findViewById(R.id.bottom_sheet);
        title = findViewById(R.id.title);
        ButterKnife.bind(this);
        createitems();
        mSheetLayout.setFab(menu);
        mSheetLayout.setFabAnimationEndListener(this);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        ahBottomNavigation.setOnTabSelectedListener(this);
        ahBottomNavigation.setAccentColor(getResources().getColor(R.color.colorPrimary));
        // revealFAB.setIntent(new Intent(MainActivity.this, Sample.class));

       /* revealFAB.setOnClickListener(new RevealFAB.OnClickListener() {
            @Override
            public void onClick(RevealFAB button, View v) {
                button.startActivityWithAnimation();
            }
        });*/
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });
        fragment = new HomeFragment();
        replaceFragment(fragment);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSheetLayout.expandFab();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // revealFAB.onResume();
    }

    @Override
    public void onFabAnimationEnd() {
        Intent intent = new Intent(this, SidemenuActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestcode", "onActivityResult: requestcode" + requestCode);
        Log.e("resultcode", "onActivityResult: " + resultCode);

        Log.e("data", "onActivityResult: " + data);

        if (requestCode == REQUEST_CODE) {
            try {
                String returnValue = data.getStringExtra("type");
                if (returnValue.equals("member")) {
                    fragment = new MemberlistFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.member_directory));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("event")) {
                    fragment = new EventFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.events));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("contactus")) {
                    fragment = new ContactFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.contact_us));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("about")) {
                    fragment = new AboutFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.about));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("gallery")) {
                    fragment = new GalleryFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.gallery));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("projects")) {
                    fragment = new ProjectListFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.project));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("connect")) {
                    fragment = new ConnectFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.connect));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("video")) {
                    fragment = new VideoFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.video));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("news")) {
                    fragment = new NewsFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.news));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("music")) {
                    fragment = new MusicPlayerFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.music));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("post")) {
                    fragment = new PostFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.music));
                    mSheetLayout.contractFab();
                }
            } catch (Exception e) {
                e.printStackTrace();
                mSheetLayout.contractFab();
            }
        }
    }

    private void createitems() {

        AHBottomNavigationItem homeitem = new AHBottomNavigationItem(getString(R.string.home), R.drawable.ic_dashboard_home);
        AHBottomNavigationItem theatreitem = new AHBottomNavigationItem(getString(R.string.member_directory), R.drawable.ic_dashboard_memeber);
        AHBottomNavigationItem searchitem = new AHBottomNavigationItem(getString(R.string.gallery), R.drawable.ic_dashboard_gallery);
        ahBottomNavigation.addItem(homeitem);
        ahBottomNavigation.addItem(theatreitem);
        ahBottomNavigation.addItem(searchitem);
        ahBottomNavigation.setCurrentItem(0);
        ahBottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        ahBottomNavigation.setTranslucentNavigationEnabled(true);

     /*  setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
               getSupportActionBar().setTitle("Club App");
        }*/
    }

    public static void setTitle(String msg) {
        title.setText(msg);
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        if (position == 0) {
            fragment = new HomeFragment();
            replaceFragment(fragment);
            setTitle(getResources().getString(R.string.app_name));
        } else if (position == 1) {
            fragment = new MemberlistFragment();
            replaceFragment(fragment);
            setTitle(getResources().getString(R.string.member_directory));
        } else if (position == 2) {
            fragment = new GalleryFragment();
            replaceFragment(fragment);
            setTitle(getResources().getString(R.string.gallery));
        }
        return true;
    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.main, fragment, "");
            fragmentTransaction.commit();
        }
    }
}