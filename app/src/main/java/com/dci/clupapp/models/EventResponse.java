package com.dci.clupapp.models;

import java.util.List;

public class EventResponse {
	public String Status;
	public EventResults Results;

	public void setStatus(String status) {
		this.Status = status;
	}

	public String getStatus() {
		return Status;
	}

	public void setResults(EventResults results) {
		this.Results = results;
	}

	public EventResults getResults() {
		return Results;
	}


	public class EventResults {
		private String firstPageUrl;
		private String path;
		private String perPage;
		private int total;
		private List<EventDataItem> data;
		private int lastPage;
		private String lastPageUrl;
		private Object nextPageUrl;
		private int from;
		private int to;

		public String getImage() {
			return Image;
		}

		public void setImage(String image) {
			Image = image;
		}

		private String Image;
		private Object prevPageUrl;
		private int currentPage;

		public void setFirstPageUrl(String firstPageUrl) {
			this.firstPageUrl = firstPageUrl;
		}

		public String getFirstPageUrl() {
			return firstPageUrl;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getPath() {
			return path;
		}

		public void setPerPage(String perPage) {
			this.perPage = perPage;
		}

		public String getPerPage() {
			return perPage;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public int getTotal() {
			return total;
		}

		public void setData(List<EventDataItem> data) {
			this.data = data;
		}

		public List<EventDataItem> getData() {
			return data;
		}

		public void setLastPage(int lastPage) {
			this.lastPage = lastPage;
		}

		public int getLastPage() {
			return lastPage;
		}

		public void setLastPageUrl(String lastPageUrl) {
			this.lastPageUrl = lastPageUrl;
		}

		public String getLastPageUrl() {
			return lastPageUrl;
		}

		public void setNextPageUrl(Object nextPageUrl) {
			this.nextPageUrl = nextPageUrl;
		}

		public Object getNextPageUrl() {
			return nextPageUrl;
		}

		public void setFrom(int from) {
			this.from = from;
		}

		public int getFrom() {
			return from;
		}

		public void setTo(int to) {
			this.to = to;
		}

		public int getTo() {
			return to;
		}

		public void setPrevPageUrl(Object prevPageUrl) {
			this.prevPageUrl = prevPageUrl;
		}

		public Object getPrevPageUrl() {
			return prevPageUrl;
		}

		public void setCurrentPage(int currentPage) {
			this.currentPage = currentPage;
		}

		public int getCurrentPage() {
			return currentPage;
		}

	}

	public class EventDataItem{
		private String venue;
		private EventDataItem.Country country;
		private String address2;
		private EventDataItem.City city;
		private String address1;
		private String latitude;
		private String description;
		private String createdAt;
		private String updated_at;
		private EventDataItem.Vendor vendor;
		private int vendorId;
		private String name;
		private int id;
		private String Image;

		public String getImage() {
			return Image;
		}

		public void setImage(String image) {
			Image = image;
		}

		private int stateId;
		private EventDataItem.State state;
		private int countryId;
		private int cityId;
		private String longitude;
		private int status;

		public void setVenue(String venue){
			this.venue = venue;
		}

		public String getVenue(){
			return venue;
		}

		public void setCountry(EventDataItem.Country country){
			this.country = country;
		}

		public EventDataItem.Country getCountry(){
			return country;
		}

		public void setAddress2(String address2){
			this.address2 = address2;
		}

		public String getAddress2(){
			return address2;
		}

		public void setCity(EventDataItem.City city){
			this.city = city;
		}

		public EventDataItem.City getCity(){
			return city;
		}

		public void setAddress1(String address1){
			this.address1 = address1;
		}

		public String getAddress1(){
			return address1;
		}

		public void setLatitude(String latitude){
			this.latitude = latitude;
		}

		public String getLatitude(){
			return latitude;
		}

		public void setDescription(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}

		public void setCreatedAt(String createdAt){
			this.createdAt = createdAt;
		}

		public String getCreatedAt(){
			return createdAt;
		}

		public void setUpdatedAt(String updatedAt){
			this.updated_at = updatedAt;
		}

		public String getUpdatedAt(){
			return updated_at;
		}

		public void setVendor(EventDataItem.Vendor vendor){
			this.vendor = vendor;
		}

		public EventDataItem.Vendor getVendor(){
			return vendor;
		}

		public void setVendorId(int vendorId){
			this.vendorId = vendorId;
		}

		public int getVendorId(){
			return vendorId;
		}

		public void setName(String name){
			this.name = name;
		}

		public String getName(){
			return name;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setStateId(int stateId){
			this.stateId = stateId;
		}

		public int getStateId(){
			return stateId;
		}

		public void setState(EventDataItem.State state){
			this.state = state;
		}

		public EventDataItem.State getState(){
			return state;
		}

		public void setCountryId(int countryId){
			this.countryId = countryId;
		}

		public int getCountryId(){
			return countryId;
		}

		public void setCityId(int cityId){
			this.cityId = cityId;
		}

		public int getCityId(){
			return cityId;
		}

		public void setLongitude(String longitude){
			this.longitude = longitude;
		}

		public String getLongitude(){
			return longitude;
		}

		public void setStatus(int status){
			this.status = status;
		}

		public int getStatus(){
			return status;
		}

		public class City{
			private String name;
			private int id;
			private int stateId;
			private int countryId;
			private int status;

			public void setName(String name){
				this.name = name;
			}

			public String getName(){
				return name;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setStateId(int stateId){
				this.stateId = stateId;
			}

			public int getStateId(){
				return stateId;
			}

			public void setCountryId(int countryId){
				this.countryId = countryId;
			}

			public int getCountryId(){
				return countryId;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}
		}
		public class Country{
			private String updatedAt;
			private String name;
			private String createdAt;
			private int id;
			private int status;

			public void setUpdatedAt(String updatedAt){
				this.updatedAt = updatedAt;
			}

			public String getUpdatedAt(){
				return updatedAt;
			}

			public void setName(String name){
				this.name = name;
			}

			public String getName(){
				return name;
			}

			public void setCreatedAt(String createdAt){
				this.createdAt = createdAt;
			}

			public String getCreatedAt(){
				return createdAt;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}
		}
		public class Vendor{
			private String firstmame;
			private String address2;
			private String address1;
			private String mobile;
			private String createdAt;
			private String lastname;
			private int categoryId;
			private String updatedAt;
			private String phone;
			private String companyname;
			private int id;
			private int stateId;
			private int activatedOn;
			private int countryId;
			private String email;
			private int planId;
			private int cityId;
			private int status;

			public void setFirstmame(String firstmame){
				this.firstmame = firstmame;
			}

			public String getFirstmame(){
				return firstmame;
			}

			public void setAddress2(String address2){
				this.address2 = address2;
			}

			public String getAddress2(){
				return address2;
			}

			public void setAddress1(String address1){
				this.address1 = address1;
			}

			public String getAddress1(){
				return address1;
			}

			public void setMobile(String mobile){
				this.mobile = mobile;
			}

			public String getMobile(){
				return mobile;
			}

			public void setCreatedAt(String createdAt){
				this.createdAt = createdAt;
			}

			public String getCreatedAt(){
				return createdAt;
			}

			public void setLastname(String lastname){
				this.lastname = lastname;
			}

			public String getLastname(){
				return lastname;
			}

			public void setCategoryId(int categoryId){
				this.categoryId = categoryId;
			}

			public int getCategoryId(){
				return categoryId;
			}

			public void setUpdatedAt(String updatedAt){
				this.updatedAt = updatedAt;
			}

			public String getUpdatedAt(){
				return updatedAt;
			}

			public void setPhone(String phone){
				this.phone = phone;
			}

			public String getPhone(){
				return phone;
			}

			public void setCompanyname(String companyname){
				this.companyname = companyname;
			}

			public String getCompanyname(){
				return companyname;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setStateId(int stateId){
				this.stateId = stateId;
			}

			public int getStateId(){
				return stateId;
			}

			public void setActivatedOn(int activatedOn){
				this.activatedOn = activatedOn;
			}

			public int getActivatedOn(){
				return activatedOn;
			}

			public void setCountryId(int countryId){
				this.countryId = countryId;
			}

			public int getCountryId(){
				return countryId;
			}

			public void setEmail(String email){
				this.email = email;
			}

			public String getEmail(){
				return email;
			}

			public void setPlanId(int planId){
				this.planId = planId;
			}

			public int getPlanId(){
				return planId;
			}

			public void setCityId(int cityId){
				this.cityId = cityId;
			}

			public int getCityId(){
				return cityId;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}
		}

		public class State{
			private String updatedAt;
			private String name;
			private String createdAt;
			private int id;
			private int countryId;
			private int status;

			public void setUpdatedAt(String updatedAt){
				this.updatedAt = updatedAt;
			}

			public String getUpdatedAt(){
				return updatedAt;
			}

			public void setName(String name){
				this.name = name;
			}

			public String getName(){
				return name;
			}

			public void setCreatedAt(String createdAt){
				this.createdAt = createdAt;
			}

			public String getCreatedAt(){
				return createdAt;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setCountryId(int countryId){
				this.countryId = countryId;
			}

			public int getCountryId(){
				return countryId;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}
		}
	}

}
