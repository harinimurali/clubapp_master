package com.dci.clupapp.models;

public class ProfileResponse {
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public ProfileRes getResults() {
		return Results;
	}

	public void setResults(ProfileRes results) {
		Results = results;
	}

	private String Status;
	public ProfileRes Results;

	public class ProfileRes {
		private String firstname;
		private String gender;
		private String appVersion;
		private String latitude;
		private String createdAt;
		private String weddingAnniversary;
		private String fcm;
		private String profile_image;
		private String deviceName;
		private String updatedAt;
		private int classificationId;
		private int id;
		private int stateId;
		private String email;
		private String longitude;
		private String deviceId;
		private String address2;
		private String address1;
		private String appOs;
		private String mobile;

		public String getFirstname() {
			return firstname;
		}

		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getAppVersion() {
			return appVersion;
		}

		public void setAppVersion(String appVersion) {
			this.appVersion = appVersion;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getWeddingAnniversary() {
			return weddingAnniversary;
		}

		public void setWeddingAnniversary(String weddingAnniversary) {
			this.weddingAnniversary = weddingAnniversary;
		}

		public String getFcm() {
			return fcm;
		}

		public void setFcm(String fcm) {
			this.fcm = fcm;
		}

		public String getProfileImage() {
			return profile_image;
		}

		public void setProfileImage(String profileImage) {
			this.profile_image = profileImage;
		}

		public String getDeviceName() {
			return deviceName;
		}

		public void setDeviceName(String deviceName) {
			this.deviceName = deviceName;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public int getClassificationId() {
			return classificationId;
		}

		public void setClassificationId(int classificationId) {
			this.classificationId = classificationId;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getStateId() {
			return stateId;
		}

		public void setStateId(int stateId) {
			this.stateId = stateId;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public String getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}

		public String getAddress2() {
			return address2;
		}

		public void setAddress2(String address2) {
			this.address2 = address2;
		}

		public String getAddress1() {
			return address1;
		}

		public void setAddress1(String address1) {
			this.address1 = address1;
		}

		public String getAppOs() {
			return appOs;
		}

		public void setAppOs(String appOs) {
			this.appOs = appOs;
		}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public String getSpousename() {
			return spousename;
		}

		public void setSpousename(String spousename) {
			this.spousename = spousename;
		}

		public String getOtp() {
			return otp;
		}

		public void setOtp(String otp) {
			this.otp = otp;
		}

		public String getLastname() {
			return lastname;
		}

		public void setLastname(String lastname) {
			this.lastname = lastname;
		}

		public String getMaritalStatus() {
			return maritalStatus;
		}

		public void setMaritalStatus(String maritalStatus) {
			this.maritalStatus = maritalStatus;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getDob() {
			return dob;
		}

		public void setDob(String dob) {
			this.dob = dob;
		}

		public int getVendorId() {
			return vendor_id;
		}

		public void setVendorId(int vendorId) {
			this.vendor_id = vendorId;
		}

		public String getOnlinestatus() {
			return onlinestatus;
		}

		public void setOnlinestatus(String onlinestatus) {
			this.onlinestatus = onlinestatus;
		}

		public String getImei() {
			return imei;
		}

		public void setImei(String imei) {
			this.imei = imei;
		}

		public String getOtpsenttime() {
			return otpsenttime;
		}

		public void setOtpsenttime(String otpsenttime) {
			this.otpsenttime = otpsenttime;
		}

		public int getCountryId() {
			return countryId;
		}

		public void setCountryId(int countryId) {
			this.countryId = countryId;
		}

		public int getPositionId() {
			return positionId;
		}

		public void setPositionId(int positionId) {
			this.positionId = positionId;
		}

		public int getCityId() {
			return cityId;
		}

		public void setCityId(int cityId) {
			this.cityId = cityId;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		private String spousename;
		private String otp;
		private String lastname;
		private String maritalStatus;
		private String phone;
		private String dob;
		private int vendor_id;
		private String onlinestatus;
		private String imei;
		private String otpsenttime;
		private int countryId;
		private int positionId;
		private int cityId;
		private int status;
	}
}
