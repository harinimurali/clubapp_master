package com.dci.clupapp.models;

/**
 * Created by keerthana on 11/8/2018.
 */

public class EventDTO {

    private String img,title,desc,date,time,location_txt;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }




    public String getLocation_txt() {
        return location_txt;
    }

    public void setLocation_txt(String location_txt) {
        this.location_txt = location_txt;
    }



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
