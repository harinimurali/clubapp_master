package com.dci.clupapp.models;

/**
 * Created by keerthana on 11/19/2018.
 */

public class VideoDTO {

    private String img,txt,views,status,time,URL;



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }



    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }




    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }




}
