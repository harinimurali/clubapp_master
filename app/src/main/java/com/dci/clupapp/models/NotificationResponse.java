package com.dci.clupapp.models;

import java.util.List;

public class NotificationResponse{
	private String Status;
	public NotificationResults Results;
	public void setStatus(String status){
		this.Status = status;
	}

	public String getStatus(){
		return Status;
	}

	public void setResults(NotificationResults results) {
		this.Results = results;
	}

	public NotificationResults getResults() {
		return Results;
	}




	public class NotificationResults{
		private String path;
		private String perPage;
		private int total;
		private List<NotificationDataItem> data;
		private int lastPage;
		private Object nextPageUrl;
		private int from;
		private int to;
		private Object prevPageUrl;
		private int currentPage;

		public void setPath(String path){
			this.path = path;
		}

		public String getPath(){
			return path;
		}

		public void setPerPage(String perPage){
			this.perPage = perPage;
		}

		public String getPerPage(){
			return perPage;
		}

		public void setTotal(int total){
			this.total = total;
		}

		public int getTotal(){
			return total;
		}

		public void setData(List<NotificationDataItem> data) {
			this.data = data;
		}

		public List<NotificationDataItem> getData() {
			return data;
		}

		public void setLastPage(int lastPage){
			this.lastPage = lastPage;
		}

		public int getLastPage(){
			return lastPage;
		}

		public void setNextPageUrl(Object nextPageUrl){
			this.nextPageUrl = nextPageUrl;
		}

		public Object getNextPageUrl(){
			return nextPageUrl;
		}

		public void setFrom(int from){
			this.from = from;
		}

		public int getFrom(){
			return from;
		}

		public void setTo(int to){
			this.to = to;
		}

		public int getTo(){
			return to;
		}

		public void setPrevPageUrl(Object prevPageUrl){
			this.prevPageUrl = prevPageUrl;
		}

		public Object getPrevPageUrl(){
			return prevPageUrl;
		}

		public void setCurrentPage(int currentPage){
			this.currentPage = currentPage;
		}

		public int getCurrentPage(){
			return currentPage;
		}



	}
	public class NotificationDataItem{
		private String venue;
		private NotificationDataItem.Users users;
		private NotificationDataItem.Vendors Vendors;
		private String address2;
		private String message;
		private String address1;
		private String latitude;
		private String description;
		private String createdAt;
		private String updated_at;
		private Vendors vendor;
		private int vendorId;
		private String name;
		private int id;
		private int stateId;
		private int countryId;
		private int cityId;
		private String longitude;
		private int status;

		public void setVenue(String venue){
			this.venue = venue;
		}

		public String getVenue(){
			return venue;
		}

		public void setMessage(String message){
			this.message = message;
		}

		public String getMessage(){
			return message;
		}

		public void setUsers(NotificationDataItem.Users users){

			this.users = users;
		}

		public NotificationDataItem.Users getUsers(){
			return users;
		}


		public void setAddress2(String address2){
			this.address2 = address2;
		}

		public String getAddress2(){
			return address2;
		}



		public void setAddress1(String address1){
			this.address1 = address1;
		}

		public String getAddress1(){
			return address1;
		}

		public void setLatitude(String latitude){
			this.latitude = latitude;
		}

		public String getLatitude(){
			return latitude;
		}

		public void setDescription(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}

		public void setCreatedAt(String createdAt){
			this.createdAt = createdAt;
		}

		public String getCreatedAt(){
			return createdAt;
		}

		public void setUpdatedAt(String updatedAt){
			this.updated_at = updatedAt;
		}

		public String getUpdatedAt(){
			return updated_at;
		}

		public void setVendor(Vendors vendor){
			this.vendor = vendor;
		}

		public Vendors getVendor(){
			return vendor;
		}

		public void setVendorId(int vendorId){
			this.vendorId = vendorId;
		}

		public int getVendorId(){
			return vendorId;
		}

		public void setName(String name){
			this.name = name;
		}

		public String getName(){
			return name;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setStateId(int stateId){
			this.stateId = stateId;
		}

		public int getStateId(){
			return stateId;
		}

		public void setCountryId(int countryId){
			this.countryId = countryId;
		}

		public int getCountryId(){
			return countryId;
		}

		public void setCityId(int cityId){
			this.cityId = cityId;
		}

		public int getCityId(){
			return cityId;
		}

		public void setLongitude(String longitude){
			this.longitude = longitude;
		}

		public String getLongitude(){
			return longitude;
		}

		public void setStatus(int status){
			this.status = status;
		}

		public int getStatus(){
			return status;
		}

		public class Users{
			private String firstname;
			private String gender;
			private String appVersion;
			private String latitude;
			private String createdAt;
			private String weddingAnniversary;
			private String fcm;
			private String profileImage;
			private String deviceName;
			private String updatedAt;
			private int classificationId;
			private int id;
			private int stateId;
			private String email;
			private String longitude;
			private String deviceId;
			private String address2;
			private String address1;
			private String appOs;
			private String mobile;
			private String spousename;
			private String otp;
			private String lastname;
			private String maritalStatus;
			private String phone;
			private String dob;
			private int vendorId;
			private String onlinestatus;
			private String imei;
			private String otpsenttime;
			private int countryId;
			private int positionId;
			private int cityId;
			private int status;

			public void setFirstname(String firstname){
				this.firstname = firstname;
			}

			public String getFirstname(){
				return firstname;
			}

			public void setGender(String gender){
				this.gender = gender;
			}

			public String getGender(){
				return gender;
			}

			public void setAppVersion(String appVersion){
				this.appVersion = appVersion;
			}

			public String getAppVersion(){
				return appVersion;
			}

			public void setLatitude(String latitude){
				this.latitude = latitude;
			}

			public String getLatitude(){
				return latitude;
			}

			public void setCreatedAt(String createdAt){
				this.createdAt = createdAt;
			}

			public String getCreatedAt(){
				return createdAt;
			}

			public void setWeddingAnniversary(String weddingAnniversary){
				this.weddingAnniversary = weddingAnniversary;
			}

			public String getWeddingAnniversary(){
				return weddingAnniversary;
			}

			public void setFcm(String fcm){
				this.fcm = fcm;
			}

			public String getFcm(){
				return fcm;
			}

			public void setProfileImage(String profileImage){
				this.profileImage = profileImage;
			}

			public String getProfileImage(){
				return profileImage;
			}

			public void setDeviceName(String deviceName){
				this.deviceName = deviceName;
			}

			public String getDeviceName(){
				return deviceName;
			}

			public void setUpdatedAt(String updatedAt){
				this.updatedAt = updatedAt;
			}

			public String getUpdatedAt(){
				return updatedAt;
			}

			public void setClassificationId(int classificationId){
				this.classificationId = classificationId;
			}

			public int getClassificationId(){
				return classificationId;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setStateId(int stateId){
				this.stateId = stateId;
			}

			public int getStateId(){
				return stateId;
			}

			public void setEmail(String email){
				this.email = email;
			}

			public String getEmail(){
				return email;
			}

			public void setLongitude(String longitude){
				this.longitude = longitude;
			}

			public String getLongitude(){
				return longitude;
			}

			public void setDeviceId(String deviceId){
				this.deviceId = deviceId;
			}

			public String getDeviceId(){
				return deviceId;
			}

			public void setAddress2(String address2){
				this.address2 = address2;
			}

			public String getAddress2(){
				return address2;
			}

			public void setAddress1(String address1){
				this.address1 = address1;
			}

			public String getAddress1(){
				return address1;
			}

			public void setAppOs(String appOs){
				this.appOs = appOs;
			}

			public String getAppOs(){
				return appOs;
			}

			public void setMobile(String mobile){
				this.mobile = mobile;
			}

			public String getMobile(){
				return mobile;
			}

			public void setSpousename(String spousename){
				this.spousename = spousename;
			}

			public String getSpousename(){
				return spousename;
			}

			public void setOtp(String otp){
				this.otp = otp;
			}

			public String getOtp(){
				return otp;
			}

			public void setLastname(String lastname){
				this.lastname = lastname;
			}

			public String getLastname(){
				return lastname;
			}

			public void setMaritalStatus(String maritalStatus){
				this.maritalStatus = maritalStatus;
			}

			public String getMaritalStatus(){
				return maritalStatus;
			}

			public void setPhone(String phone){
				this.phone = phone;
			}

			public String getPhone(){
				return phone;
			}

			public void setDob(String dob){
				this.dob = dob;
			}

			public String getDob(){
				return dob;
			}

			public void setVendorId(int vendorId){
				this.vendorId = vendorId;
			}

			public int getVendorId(){
				return vendorId;
			}

			public void setOnlinestatus(String onlinestatus){
				this.onlinestatus = onlinestatus;
			}

			public String getOnlinestatus(){
				return onlinestatus;
			}

			public void setImei(String imei){
				this.imei = imei;
			}

			public String getImei(){
				return imei;
			}

			public void setOtpsenttime(String otpsenttime){
				this.otpsenttime = otpsenttime;
			}

			public String getOtpsenttime(){
				return otpsenttime;
			}

			public void setCountryId(int countryId){
				this.countryId = countryId;
			}

			public int getCountryId(){
				return countryId;
			}

			public void setPositionId(int positionId){
				this.positionId = positionId;
			}

			public int getPositionId(){
				return positionId;
			}

			public void setCityId(int cityId){
				this.cityId = cityId;
			}

			public int getCityId(){
				return cityId;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}


		}

		public class Vendors{
			private String firstmame;
			private String address2;
			private String address1;
			private String mobile;
			private String createdAt;
			private String lastname;
			private int categoryId;
			private String updatedAt;
			private String phone;
			private String companyname;
			private int id;
			private int stateId;
			private int activatedOn;
			private int countryId;
			private String email;
			private int planId;
			private int cityId;
			private int status;

			public void setFirstmame(String firstmame){
				this.firstmame = firstmame;
			}

			public String getFirstmame(){
				return firstmame;
			}

			public void setAddress2(String address2){
				this.address2 = address2;
			}

			public String getAddress2(){
				return address2;
			}

			public void setAddress1(String address1){
				this.address1 = address1;
			}

			public String getAddress1(){
				return address1;
			}

			public void setMobile(String mobile){
				this.mobile = mobile;
			}

			public String getMobile(){
				return mobile;
			}

			public void setCreatedAt(String createdAt){
				this.createdAt = createdAt;
			}

			public String getCreatedAt(){
				return createdAt;
			}

			public void setLastname(String lastname){
				this.lastname = lastname;
			}

			public String getLastname(){
				return lastname;
			}

			public void setCategoryId(int categoryId){
				this.categoryId = categoryId;
			}

			public int getCategoryId(){
				return categoryId;
			}

			public void setUpdatedAt(String updatedAt){
				this.updatedAt = updatedAt;
			}

			public String getUpdatedAt(){
				return updatedAt;
			}

			public void setPhone(String phone){
				this.phone = phone;
			}

			public String getPhone(){
				return phone;
			}

			public void setCompanyname(String companyname){
				this.companyname = companyname;
			}

			public String getCompanyname(){
				return companyname;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setStateId(int stateId){
				this.stateId = stateId;
			}

			public int getStateId(){
				return stateId;
			}

			public void setActivatedOn(int activatedOn){
				this.activatedOn = activatedOn;
			}

			public int getActivatedOn(){
				return activatedOn;
			}

			public void setCountryId(int countryId){
				this.countryId = countryId;
			}

			public int getCountryId(){
				return countryId;
			}

			public void setEmail(String email){
				this.email = email;
			}

			public String getEmail(){
				return email;
			}

			public void setPlanId(int planId){
				this.planId = planId;
			}

			public int getPlanId(){
				return planId;
			}

			public void setCityId(int cityId){
				this.cityId = cityId;
			}

			public int getCityId(){
				return cityId;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}


		}


	}


}