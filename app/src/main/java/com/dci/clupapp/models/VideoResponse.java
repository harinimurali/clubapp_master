package com.dci.clupapp.models;

import java.util.List;

public class VideoResponse{
    private String Status;
    public VideoResults Results;

    public void setStatus(String status){
        this.Status = status;
    }

    public String getStatus(){
        return Status;
    }

    public void setResults(VideoResults results){
        this.Results = results;
    }

    public VideoResults  getResults(){
        return Results;
    }

    public class VideoResults{
        private String path;
        private String perPage;
        private int total;
        public List<VideoDataItem> data;
        private int lastPage;
        private String nextPageUrl;
        private int from;
        private int to;
        private String prevPageUrl;
        private int currentPage;

        public void setPath(String path){
            this.path = path;
        }

        public String getPath(){
            return path;
        }

        public void setPerPage(String perPage){
            this.perPage = perPage;
        }

        public String getPerPage(){
            return perPage;
        }

        public void setTotal(int total){
            this.total = total;
        }

        public int getTotal(){
            return total;
        }

        public void setData(List<VideoDataItem> data){
            this.data = data;
        }

        public List<VideoDataItem> getData(){
            return data;
        }

        public void setLastPage(int lastPage){
            this.lastPage = lastPage;
        }

        public int getLastPage(){
            return lastPage;
        }

        public void setNextPageUrl(String nextPageUrl){
            this.nextPageUrl = nextPageUrl;
        }

        public String getNextPageUrl(){
            return nextPageUrl;
        }

        public void setFrom(int from){
            this.from = from;
        }

        public int getFrom(){
            return from;
        }

        public void setTo(int to){
            this.to = to;
        }

        public int getTo(){
            return to;
        }

        public void setPrevPageUrl(String prevPageUrl){
            this.prevPageUrl = prevPageUrl;
        }

        public String getPrevPageUrl(){
            return prevPageUrl;
        }

        public void setCurrentPage(int currentPage){
            this.currentPage = currentPage;
        }

        public int getCurrentPage(){
            return currentPage;
        }

        public class VideoDataItem{
            private String url;
            private String updated_at;
            private String fileType;
            private int vendorId;
            private String createdAt;
            private int id;
            private String title;
            private int status;

            public void setFilePath(String filePath){
                this.url = filePath;
            }

            public String getFilePath(){
                return url;
            }

            public void setUpdatedAt(String updatedAt){
                this.updated_at = updatedAt;
            }

            public String getUpdatedAt(){
                return updated_at;
            }

            public void setFileType(String fileType){
                this.fileType = fileType;
            }

            public String getFileType(){
                return fileType;
            }

            public void setVendorId(int vendorId){
                this.vendorId = vendorId;
            }

            public int getVendorId(){
                return vendorId;
            }

            public void setCreatedAt(String createdAt){
                this.createdAt = createdAt;
            }

            public String getCreatedAt(){
                return createdAt;
            }

            public void setId(int id){
                this.id = id;
            }

            public int getId(){
                return id;
            }

            public void setTitle(String title){
                this.title = title;
            }

            public String getTitle(){
                return title;
            }

            public void setStatus(int status){
                this.status = status;
            }

            public int getStatus(){
                return status;
            }
        }

    }
}
