package com.dci.clupapp.models;

/**
 * Created by keerthana on 12/3/2018.
 */

public class NewsTrendingDTO {

    private String image,title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
