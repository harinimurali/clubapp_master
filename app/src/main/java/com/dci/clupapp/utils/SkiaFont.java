package com.dci.clupapp.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by harini on 6/12/2018.
 */

public class SkiaFont {
    private static SkiaFont instance;
    private static Typeface typeface;
    private static Typeface typeface_semi_bold;
    private static Typeface typeface_bold;
    private static Typeface typeface_light;


    public static SkiaFont getInstance(Context context) {
        synchronized (SkiaFont.class) {
            if (instance == null) {
                instance = new SkiaFont();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "skia_regular.ttf");
                typeface_semi_bold = Typeface.createFromAsset(context.getResources().getAssets(), "skia_regular.ttf");
                typeface_bold = Typeface.createFromAsset(context.getResources().getAssets(), "UBUNTUB.TTF");
            }
            return instance;
        }
    }

    public Typeface getRegularTypeFace() {
        return typeface;
    }

    public Typeface getSemiBoldTypeFace() {
        return typeface_semi_bold;
    }

    public Typeface getLightTypeFace() {
        return typeface_light;
    }

    public Typeface getBoldTypeFace() {
        return typeface_bold;
    }
}
