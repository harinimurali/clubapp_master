package com.dci.clupapp.utils;

/**
 * Created by harini on 12/5/2018.
 */

public class Constants {
    public static final String CLUB_BASE_URL = "http://clubapp.dci.in/api/";
    public static final String SECURE_TOKEN = "SECURE_TOKEN";
    public static final String DEVICEID = "device_id";
    public static final String APPID = "app_id";
    public static final String APPVERSION = "app_version";
    public static final String DEVICEOS = "device_os";
    public static final String OSVERSION = "os_version";
    public static final String MOBILENUMBER = "mobilenum";
    public static final String PROFILE = "profile";
    public static final String FCMTOKEN = "fcmkey";
    public static final String FCMKEYSHAREDPERFRENCES = "fcmsharedpreference";
    public static final String USERID = "userid";
    public static final String VENDORID = "vendorid";
    public static final String LOGIN_STATUS = "login_status";
    public static final String PAGE = "page";
    public static final String PAGESIZE = "pagesize";
    public static final String EVENTID = "event_id";
    public static final String ImageUrl = "http://clubapp.dci.in/uploads/userimages/";
}
