package com.dci.clupapp.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harini on 8/23/2018.
 */

public class FontTextViewBold extends TextView {
    public FontTextViewBold(Context context) {
        super(context);
        setIncludeFontPadding(false);
        setTypeface(SkiaFont.getInstance(context).getBoldTypeFace());
    }

    public FontTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false);
        setTypeface(SkiaFont.getInstance(context).getBoldTypeFace());
    }

    public FontTextViewBold(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
        setTypeface(SkiaFont.getInstance(context).getBoldTypeFace());
    }
}
